import javax.swing.*;
public class HelloJava
{
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Hello, Java!");
		JLabel label_world = new JLabel("Hello World!", JLabel.CENTER); // LEFT, CENTER, RIGHT, LEADING, TRAILING

		frame.getContentPane().add(label_world);
		frame.setSize(300, 300);
		frame.setVisible(true);
	}
}